﻿using System;

namespace rmm
{
    public delegate void ChannelData(int channelNum, double[] data);
    // Interface to get access to the beats producing or receiving device with linear frequency modulation
    public abstract class IBeatsProducer
    {
        // Call configuration thingy. Idealy this returns System.Windows.Forms.Form object with GUI to configure the device.
        public abstract Object ShowConfigurationForm();
        public abstract double SamplingRate { get; }
        public abstract double FrequencyDeviation { get; }
        public abstract double MiddleFrequency { get; }
        public abstract double MinimumFrequency { get; }
        public abstract double MaximumFrequency { get; }
        public abstract double Period { get; }
        public abstract double NumOfChannels { get; }
        // Event raises when new data for channel is ready
        public abstract event ChannelData DataReady;
    }
}
